import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Scanner;
import java.lang.System;

public class MayorMenor {

    // Declaro las variables globales
    private static final String folderPath = System.getProperty("user.home") + File.separator + "Documents" + File.separator + "DavidSoft" + File.separator + "MayorMenor";
    private static final String fileName = "aciertos.txt";
    private static final String path = folderPath + File.separator + fileName;
    private static String name = "";
    private static int guesses = 0;
    private static int quantity = 10;
    private static Integer[] lista;


    public static void main(String[] args) throws IOException {
        int option;

        System.out.println("Bienvenido a este programa en el que puedes poner tu memoria y suerte a prueba.");
        System.out.println();

        do {
            menu();
            option = getOption();
            useOption(option);
        } while (option!=0);

        System.out.println();
        System.out.println("Gracias por jugar, hasta luego!");
    }

    private static void useOption(int option) throws IOException {

        // No hay que verificar que el valor sea correcto, ya que se verificó anteriormente.
        if (option == 1) {
            if (name.equals("")) name = getName();
            getRandom();
            guesses = getMayorMenor(guesses, lista);
            mensaje(guesses, name);
        }
        if (option == 2) {
            if (existe()) {
                showGuesses();
            } else {
                System.out.println("No existe el fichero con los aciertos en la ruta:");
                System.out.println(path);
                System.out.println("Guarda tus aciertos primero o juega si aun no lo has hecho.");
            }
        }
        if (option == 3) {
            if (!name.equals("")) {
                if (existe()) {
                    saveGuesses();
                } else {
                    System.out.println("Se va a crear un fichero para guardar los aciertos.");
                    if (continuar()) {
                        saveGuesses();
                    } else System.out.println("No se ha creado el fichero.");
                }
            } else System.out.println("Tienes que jugar primero para guardar tus aciertos.");
        }
        if (option == 4) {
            if (name.equals("")) {
                System.out.println("Tienes que jugar primero");
            } else {
                mensaje(guesses, name);
            }
        }
        if (option == 5) {
            if (!name.equals("") && guesses == 0) name = getName();
            if (name.equals("")) System.out.println("La primera vez que juegues te pediremos el nombre.");
            if (guesses != 0) {
                System.out.println("No se han guardado los aciertos del usuario " + name + ".");
                System.out.println("Debes guardar antes de continuar (a no ser que quieras asignar los aciertos a otro usuario).");
                if (continuar()) name = getName();
                System.out.println();
                System.out.println("Si continuas se asignaran los " + guesses + " aciertos a " + name + ".");
                System.out.println("Si no continuas, los aciertos se van a poner a cero para " + name + ".");
                if (!continuar()) guesses = 0;
            }
        }
        if (option == 6) {
            System.out.println("La cantidad de valores ahora mismo es: " + quantity);
            System.out.println("Vamos a cambiar la cantidad");
            if (continuar()) {
                cambiarCantidad();
            } else System.out.println("No se ha cambiado la cantidad de valores.");
        }
        if (option != 0) {
            System.out.println();
            espera();
            clearScreen();
        }
    }

    private static void clearScreen() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        } catch (final Exception e) {
            System.out.println("Exception: " + e);
        }
    }

    // Este método se usa para guardar los aciertos en el fichero
    private static void saveGuesses() throws IOException {
        // Creamos la carpeta donde se guardaran los resultados con el fichero de texto
        if (!existe()) createFolder();
        try {
            String[] datos = new String[2];
            datos[0] = name;
            datos[1] = String.valueOf(guesses);

            byte[] fichero = getFile();

            PrintStream out = new PrintStream(path);
            out.write(fichero);
            // Añadimos los datos nuevos al fichero.
            out.write(Arrays.toString(datos).getBytes());
            // escribimos una línea nueva:
            out.println();
            // Es de buen programador, siempre cerrar los ficheros que usamos.
            out.close();
        } catch (Exception e) {
            System.out.println("Excepción: " + e.getMessage());
        }
        System.out.println("Se han guardado los aciertos actuales en el fichero:");
        System.out.println(path);
        // ponemos los aciertos a cero para poder volver a jugar.
        guesses = 0;
    }

    // Este método lee el fichero del disco
    private static byte[] getFile() throws IOException {
        byte[] fichero;
        InputStream in = new FileInputStream(path);
        fichero = in.readAllBytes();
        in.close();
        return fichero;
    }

    private static void showGuesses() throws IOException {
        byte[] fichero = getFile();
        // Mostramos los aciertos guardados anteriormente en el fichero por consola.
        System.out.println("Se muestran los aciertos guardados en el fichero:");
        System.out.println(path);
        for (byte c : fichero) {
            System.out.print((char)c);
        }
        System.out.println();
    }

    // Aquí pedimos una de las opciones y se verifica que sea correcto el valor introducido.
    private static int getOption() {
        Scanner scanner = new Scanner(System.in);
        int option = 0;
        boolean cont = false;
        // Se verifica que se introduzca una de las opciones válidas.
        do {
            System.out.println("Por favor, elige uno (0-6): ");
            try {
                option = scanner.nextInt();
            } catch (Exception e){
                System.out.println("Debes introducir una de las opciones (0-6).");
                System.out.println();
            }
            if (option >= 0 && option <= 6) cont = true;
        } while (!cont);
        return option;
    }

    private static void menu() {
        System.out.println("Opciones:");
        System.out.println("1.- Jugar a Mayor/Menor");
        System.out.println("2.- Ver aciertos guardados");
        System.out.println("3.- Guardar tus aciertos");
        System.out.println("4.- Mostrar los aciertos actuales");
        System.out.println("5.- Cambiar tu nombre de usuario");
        System.out.println("6.- Cambiar la cantidad de valores");
        System.out.println("0.- Salir");
        System.out.println();
    }

    // Este método te muestra el mensaje con el número de aciertos, si lo hubiere
    private static void mensaje(int guesses, String name) {
        int last = lista.length - 1;
        if (guesses > 0) {
            int percentage = (guesses * 100) / last;
            System.out.println("Hola " + name + ", has acertado " + guesses + " de " + last + " veces (" + percentage + "%).");
        } else {
            System.out.println("Hola " + name + ", lamentablemente no has acertado ninguna vez, vuelve a intentarlo.");
        }
    }

    // Este es el método que te va mostrando los números y te pregunta si el siguiente será mayor o menor.
    private static int getMayorMenor(int guesses, Integer[] lista) {
        boolean cont = false;
        String line = "";
        int last = lista.length - 1;
        Scanner scanner = new Scanner(System.in);

        // Solamente continuará con el juego si es la primera vez o has guardado los aciertos.
        if (guesses == 0) {
            // Recorremos el array con los valores para mostrarlos por pantalla
            for (int i = 0; i < lista.length; i++) {
                if (i != last) { // Si no es el último valor de la lista hará esto
                    do {
                        System.out.println("El valor es : " + lista[i]);
                        System.out.println("Crees que el siguiente valor sera Mayor '+' o Menor '-' que este?");
                        System.out.println("Introduce tu respuesta: '+', '-', 'Salir': ");
                        line = scanner.nextLine();
                        // Verificamos que se introduzca lo que pedimos, si no, se vuelve a pedir
                        if (Objects.equals(line, "Salir")) {
                            System.out.println("Has elegido Salir.");
                            cont = true;
                        } else if (Objects.equals(line, "+")) {
                            if (lista[i] < lista[i + 1]) {
                                guesses += 1;
                                cont = true;
                            }
                        } else if (Objects.equals(line, "-")) {
                            if (lista[i] > lista[i + 1]) {
                                guesses += 1;
                                cont = true;
                            }
                        }
                    } while (!cont);
                } else { // Cuando es el último valor de la lista, solamente se muestra y termina.
                    System.out.println("Este es el ultimo valor de la lista: " + lista[i]);
//                    wait("Pulsa intro para continuar..");
                }
                // Comprobamos que si hemos escrito la palabra "Salir" terminamos ya el loop.
                if (Objects.equals(line, "Salir")) break;
            }
            // Como no habíamos guardado los aciertos anteriormente, no nos deja jugar.
        } else {
            System.out.println("Para poder volver a jugar, tienes que guardar tus aciertos.");
//            wait("Pulsa intro para volver al menu..");
        }
        return guesses;
    }

    // Este método se usa para esperar a que se pulse intro
    private static void espera() {
        System.out.println("Pulsa intro para volver al menu.");
        try {
            System.in.read();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    // Este método te pide el nombre o nickname
    private static String getName() {
        Scanner scanner = new Scanner(System.in);
        boolean cont = true;
        String name;
        //Esto se va a repetir mientras no se escriba nada.
        do {
            System.out.println("Por favor, introduce tu nombre o nickname: ");
            name = scanner.nextLine();
            if (name.equals("")) {
                System.out.println("Tienes que escribir un nombre o nickname.");
                System.out.println();
                cont = false;
            }
        } while (!cont);
        return name;
    }

    // Este método genera la lista del 1 al 10 y la aleatoriza.
    private static void getRandom() {
        lista = new Integer[quantity];
        // Generamos la lista de números del 1 al 10
        for (int i = 0; i < lista.length; i++) {
            lista[i] = i + 1;
        }
        // Aleatorizamos la lista de números
        Collections.shuffle(Arrays.asList(lista));
    }

    // Este método crea la estructura de carpetas y el fichero donde guardamos los resultados
    private static void createFolder() throws IOException {
        // Asignamos la estructura de carpetas y nombre de fichero
        File customDir = new File(folderPath);
        File f = new File(path);

        // Si no existe la carpeta, la creamos
        if (!customDir.exists()) customDir.mkdirs();

        // Si no existe el fichero, lo creamos
        if (!f.exists()) f.createNewFile();
    }

    // Este método comprueba si existe la ruta o fichero
    private static boolean existe(){
        boolean existe = false;

        File archivo = new File(path);
        if (archivo.exists()) existe = true;

        return existe;
    }

    // Este método te pregunta si quieres continuar o no
    private static boolean continuar(){
        Scanner scanner = new Scanner(System.in);
        String respuesta = "";
        boolean cont = false;

        do {
            System.out.println("Deseas continuar (S/N):");
            System.out.println();
            try {
                respuesta = scanner.nextLine();
            } catch (Exception e){
                System.out.println("Exception: " + e);
            }
            if (respuesta.equals("S")||respuesta.equals("s")||respuesta.equals("N")||respuesta.equals("n")) cont = true;
        } while (!cont);

        return respuesta.equals("S") || respuesta.equals("s");
    }

    // Este método se usa para cambiar la cantidad de valores en la lista del array
    private static void cambiarCantidad(){
        Scanner scanner = new Scanner(System.in);
        boolean cont = true;
        int cantidad;
        //Esto se va a repetir mientras no se escriba nada.
        do {
            System.out.println("Por favor, introduce una nueva cantidad del 10 a 100:");
            cantidad = scanner.nextInt();
            if (cantidad < 10 || cantidad > 100) {
                System.out.println("Tienes que escribir una cantidad entre 10 y 100.");
                System.out.println();
                cont = false;
            }
        } while (!cont);
        quantity = cantidad;
    }
}