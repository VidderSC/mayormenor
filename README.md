Estoy empezando a programar en Java y este es un programa de mi invención, para practicar.

MayorMenor: Tienes que adivinar si el siguiente número va a ser mayor o menor que el actual.
 - Es para la línea de comandos. 
 - Habrá un menu de opciones.
 - Nos va a pedir nuestro nombre para poder jugar.
 - Podemos cambiar el nombre sin tener que cerrar el programa.
 - Hay una lista de números definida por defecto (1-10) y los números de esa lista aparecerán aleatoriamente.
 - Se puede cambiar la cantidad de valores de la lista hasta un máximo de 100 valores.
 - Al final nos dará la cuenta total de aciertos y el porcentaje de estos.
 - Vamos a poder guardar nuestros aciertos en un fichero. (Si el fichero ya existe, añadirá una nueva línea)
 - Pedirá confirmación de si queremos crear la estructura de carpetas y el fichero. 
 - Vamos a poder imprimir por consola el fichero (si existe) con los aciertos guardados previamente.